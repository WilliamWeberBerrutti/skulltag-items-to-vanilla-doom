# Skulltag Items for Vanilla Doom

**Compatibility:** Zandronum

**Note:** This needs Skulltag Data to work!

This mod brings Skulltag items to Vanilla Doom's gameplay, such as its runes, spheres, items, and weapons. They are randomly spawned per map start.


## Play Information

Games: Doom 2

Gamemodes: Competitive (such as Deathmatch)


## Construction

Base: New mod from scratch


## Credits

Mod by: Cyantusk


## Licensing

Skulltag Items to Vanilla Doom project is licensed under the [GPL-3](http://www.gnu.org/licenses/gpl.html).
